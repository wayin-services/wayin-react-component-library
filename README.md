# Wayin React Component Library

A react component library to build custom UIs quickly and more easily.

#### Next steps

1. Write tests (probably using enzyme, possibly jsdom)
2. Write prop types for each component
3. Write more documentation around each component
4. Look into creating a docs html page that has the actual components implemented, test results, and inputs that allow you to modify the props of the components
5. Look into prettier/eslint to make sure everything is standard