'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function Button(props) {
  return _react2.default.createElement(
    'button',
    {
      onClick: props.onClick,
      className: 'xButton xCTA ' + (props.type === "submit" ? 'xSubmit' : ''),
      type: props.type === "submit" ? 'submit' : 'button'
    },
    _react2.default.createElement(
      'span',
      null,
      props.children
    )
  );
};

exports.default = Button;