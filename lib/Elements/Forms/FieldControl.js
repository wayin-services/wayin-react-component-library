'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FieldError = require('./FieldError');

var _FieldError2 = _interopRequireDefault(_FieldError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FieldControl = function (_Component) {
	_inherits(FieldControl, _Component);

	_createClass(FieldControl, null, [{
		key: 'defaultProps',
		get: function get() {
			return {
				className: "",
				size: 'medium',
				valid: true,
				errorMessages: []
			};
		}
	}]);

	function FieldControl(props) {
		_classCallCheck(this, FieldControl);

		var _this = _possibleConstructorReturn(this, (FieldControl.__proto__ || Object.getPrototypeOf(FieldControl)).call(this, props));

		_this.state = {
			size: '' + props.size.slice(0, 1).toUpperCase() + props.size.substring(1).toLowerCase()
		};
		return _this;
	}

	_createClass(FieldControl, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: 'xFieldContainer xFieldWidth' + this.state.size + ' ' + (!this.props.valid ? 'xFieldError' : '') },
				_react2.default.createElement(
					'div',
					{ className: 'xField ' + this.props.className },
					this.props.children
				),
				_react2.default.createElement(
					'div',
					{ className: 'xHelpContainer' },
					_react2.default.createElement(
						'p',
						{ className: 'xHelpLabel' },
						this.props.helpLabel
					),
					!this.props.valid && this.props.errorMessages.length ? _react2.default.createElement(_FieldError2.default, { errorMessages: this.props.errorMessages }) : ''
				)
			);
		}
	}]);

	return FieldControl;
}(_react.Component);

exports.default = FieldControl;