'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldError = function FieldError(props) {
  var generateErrorMessages = function generateErrorMessages(errorMessages) {
    return errorMessages.map(function (errorMessage, i) {
      return _react2.default.createElement(
        'li',
        { key: 'tradeError-' + i, className: 'custom-error-message' },
        errorMessage
      );
    });
  };

  return _react2.default.createElement(
    'div',
    { className: 'xErrorLabel ' + props.className, style: { display: 'block' } },
    _react2.default.createElement(
      'ul',
      null,
      generateErrorMessages(props.errorMessages)
    )
  );
};

exports.default = FieldError;