'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldLabel = function FieldLabel(props) {
  return _react2.default.createElement(
    'div',
    { className: 'xLabelContainer' },
    _react2.default.createElement(
      'label',
      { className: 'xFormLabel ' + (props.className ? props.className : 'xControlLabel'), htmlFor: props.for },
      props.children
    )
  );
};

exports.default = FieldLabel;