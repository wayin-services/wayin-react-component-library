'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Input = function Input(props) {
  return _react2.default.createElement('input', {
    type: props.type,
    id: props.name,
    name: props.name,
    placeholder: props.placeholder ? props.placeholder : props.namecharAt(0).toUpperCase() + props.name.slice(1),
    onChange: props.onChange
  });
};

exports.default = Input;