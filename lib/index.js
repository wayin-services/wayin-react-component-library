'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Forms = undefined;

var _Forms = require('./Elements/Forms');

var _Forms2 = _interopRequireDefault(_Forms);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Forms = _Forms2.default;