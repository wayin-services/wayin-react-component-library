import React from 'react'

const Button = props => (
  <button 
    onClick={props.onClick}
    className={`xButton xCTA ${props.type === "submit" ? 'xSubmit' : ''}`}
    type={props.type === "submit" ? 'submit' : 'button'}
  >
    <span>
      {props.children}
    </span>
  </button>
);

export default Button;
