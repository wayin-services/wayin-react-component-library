import React, {Component} from 'react';
import FieldError from './FieldError';

export default class FieldControl extends Component {

	static get defaultProps(){
		return {
			className: "",
			size : 'medium',
			valid: true,
			errorMessages: []
		}
	}

	constructor(props){
		super(props);

		this.state = {
			size: `${props.size.slice(0,1).toUpperCase()}${props.size.substring(1).toLowerCase()}`
		}
	}

	render(){
		return (
			<div className={`xFieldContainer xFieldWidth${this.state.size} ${!this.props.valid ? 'xFieldError' : ''}`}>
				<div className={`xField ${this.props.className}`}>
					{this.props.children}
				</div>
				<div className="xHelpContainer">
					<p className="xHelpLabel">{this.props.helpLabel}</p>
					{!this.props.valid && this.props.errorMessages.length
						? <FieldError errorMessages={this.props.errorMessages} /> 
						: ''
					}
				</div>
			</div>
		)
	}
}