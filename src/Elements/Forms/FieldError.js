import React from 'react';

const FieldError = props => {
  const generateErrorMessages = errorMessages => {
    return errorMessages.map((errorMessage, i) => {
      return (<li key={`tradeError-${i}`} className="custom-error-message">{errorMessage}</li>);
    });
  };
  
  return (
    <div className={`xErrorLabel ${props.className}`} style={{display: 'block'}}>
      <ul>
        {generateErrorMessages(props.errorMessages)}
      </ul>
    </div>
  );
}

export default FieldError;