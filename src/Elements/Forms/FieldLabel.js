import React, {Component} from 'react';

const FieldLabel = props => (
  <div className="xLabelContainer">
    <label className={`xFormLabel ${props.className ? props.className : 'xControlLabel'}`} htmlFor={props.for}>
      {props.children}
    </label>
  </div>
)

export default FieldLabel;