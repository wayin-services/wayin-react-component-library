import React, {Component} from 'react';

const FieldWrapper = props => (
  <div className={`xFieldItem ${props.className ? props.className : ''}`}>
    {props.children}
  </div>
);

export default FieldWrapper;