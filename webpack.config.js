const
	path = require('path'),
	webpack = require('webpack'),
	WebpackCopyPlugin = require('copy-webpack-plugin');

module.exports = {
	devtool: 'cheap-module-eval-source-map',
  
  entry: {
		"library.js": './src/index.js'
  },

  mode: 'development',
  
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'library.bundle.js'
  },

	module: {
		rules: [{
			test: /\.jsx?$/,
			loader: 'babel-loader',
			exclude: /node_modules/,
			query: {
				presets: ['es2015', 'react']
			},
			include: __dirname
		}]
  },
  
	devServer: {
		port: 8448,
		contentBase: '/static',
		hot: false,
		https: false,
		host: 'localhost',
		inline: true,
		before(app) {
			app.use((req, res, next) => {
				res.header('Access-Control-Allow-Origin', 'http://local.engagesciences.com:8443');
				next();
			});
		}
	},

	plugins: [
		new WebpackCopyPlugin([{
			from: './src/index.html',
			to: 'index.html'
		}]),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.DefinePlugin({
			assetPath: JSON.stringify("http://localhost:8448/img/")
		})
	],
}
