const
	path = require('path'),
	webpack = require('webpack'),
	UglifyJSPlugin = require('uglify-js-plugin');

module.exports = {
	entry: {
		"bundle.min.js":'./js/src/index.js'
  },
  
  mode: 'production',

	output: {
		path: path.join(__dirname,'dist'),
		filename: '[name]',
		publicPath:'/static/'
  },
  
	plugins: [
		new UglifyJSPlugin(),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('production'),
			'assetPath': JSON.stringify("//d3uf2ssic990td.cloudfront.net/client/quickenloans/fantasy-stocks/img/")
		})
  ],
  
	module: {
		rules: [{
			test: /\.jsx?$/,
			loader: 'babel-loader',
			exclude: /node_modules/,
			query: {
				presets: ['es2015','react']
			},
			include: __dirname
		}]
	}
}